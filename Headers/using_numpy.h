double cnumpy_norm(double *npyInArray1D, int npyLength1D);
double cnumpy_dot(double *npyInArray1D_1, double *npyInArray1D_2, int npyLength1D);
void cnumpy_normalize_inplace(double *npyInPlArray1D, int npyLength1D);
void cnumpy_normalize_inplace2D(double *npyInPlArray2D, int npyLength1D, int npyLength2D);
void cnumpy_ones_argout(double* npyOutArray1D, int npyOutLength1D);
void cnumpy_normalize_argout(double* npyOutArray1D, int npyOutLength1D, double *npyInArray1D, int npyInLength1D);
void cnumpy_add(double* npyOutArray1D, int npyOutLength1D, double *npyInArray1D_1, int npyLength1D_1, double *npyInArray1D_2, int npyLength1D_2);
