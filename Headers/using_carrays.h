void print_array(double *doubleArray, int dim);
double compute_norm(double *doubleArray, int dim);
void normalize(double *doubleArray, int dim);
