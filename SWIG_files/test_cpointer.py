import swigdemo as s
import numpy as np

################################
# # test cpointer
################################
print("\nTESTING CPOINTER")

# # tests if %pointer_functions(int, intp); is used
# rp = s.new_intp()
# s.add(1,2,rp)
# print("\nrp={}".format(s.intp_value(rp)))

# # tests if %pointer_class(int, intp); is used
rp = s.intp()
s.add(1,2,rp)
print("\nrp={}\n".format(rp.value()))
