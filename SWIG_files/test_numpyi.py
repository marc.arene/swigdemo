import swigdemo as s
import numpy as np

################################
# # test using_numpy functions
################################
print("\nTESTING NUMPY.I FUNCTIONS")

array_py = [3.0, 4.0, 5.0, 5.0, 4.0, 3.0]
print("- INPUT ARRAYS typemaps:")
# Note how thanks to the typemap used, this function only takes one input as argument
print(s.cnumpy_norm(array_py))

a = [1.0, 1.0, 2.0]
b = [1.0, 10.0, 5.0]
# c = [0.0, 0.0, 100.0]
print(s.cnumpy_dot(a, b))



print("- INPLACE ARRAYS typemaps:")
# here a numpy array is needed when for the previous function a simple list also worked
array_numpy = np.array([3.0, 4.0, 5.0, 5.0, 4.0, 3.0])

print("-- 1D example")
s.cnumpy_normalize_inplace(array_numpy)
print(array_numpy)

print("-- 2D example")
array2D_numpy = np.array([[3.0, 4.0, 0.0],[6.0, 0.0, 8.0]])
s.cnumpy_normalize_inplace2D(array2D_numpy)
print(array2D_numpy)


print("- ARGOUT ARRAYS typemaps:")
# here, thanks to our typemaps, the function returns an output
print("-- Output array from integer input")
ones = s.cnumpy_ones_argout(8)
print(ones)


print("-- Output array from input array and integer input")
array_numpy = np.array([3.0, 4.0, 5.0, 5.0, 4.0, 3.0])
array_numpy_normalized = s.cnumpy_normalize_argout(6,array_numpy)
print("array_numpy = {}".format(array_numpy))
print("array_numpy_normalized = {}".format(array_numpy_normalized))

print("-- Output array from 2 input arrays")
a = np.array([1.0, 1.0, 2.0])
b = np.array([1.0, 10.0, 5.0])
print(s.cnumpy_add(3, a, b))
