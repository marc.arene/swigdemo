import swigdemo as s
import numpy as np

################################
# # test using_carrays functions
################################
print("\nTESTING CARRAYS FUNCTIONS")
# Define some functions useful for our test purposes
def set_carray_from_array(carray, array):
    for i in range(len(array)):
        carray[i] = array[i]

def create_array_from_carray(carray, length):
    array = np.empty(length)
    for i in range(length):
        array[i] = carray[i]
    return array

# Testing compute_norm(): array_c is used as an input to compute some third quantity
array_py = [3, 4, 5, 5, 4, 3]
array_c = s.doubleArray(6)
set_carray_from_array(array_c, array_py)
s.print_array(array_c, 6)
print("Norm of [3, 4, 5, 5, 4, 3] = {}".format(s.compute_norm(array_c, 6)))

# Testing normalize(): here array_c is modified
s.normalize(array_c, 6)
array_normalized_py = create_array_from_carray(array_c, 6)
print(array_normalized_py)
