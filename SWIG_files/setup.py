from distutils.core import setup, Extension
import numpy

extension_mod = Extension(
    name = "_swigdemo",
    sources = [
    "swigdemo_wrap.c",
    "../Library/basic_functions.c",
    "../Library/using_cpointer.c",
    "../Library/using_carrays.c",
    "../Library/using_numpy.c"
    ],
    include_dirs=[numpy.get_include()]
    )

# name in 'setup()' must be the same as the one above in 'Extension()' but without the underscore at the beginning, and it must also be the same as the name defined after '%module' in the interface file. This will be the name of the python module to import
setup(name = "swigdemo", ext_modules=[extension_mod])
