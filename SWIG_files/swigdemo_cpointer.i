// %module swigdemo
%include "cpointer.i"

%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  #include "../Headers/using_cpointer.h"
%}

/* Create some functions for working with "int *" */
// %pointer_functions(int, intp);


/* Wrap a class interface around an "int *" */
%pointer_class(int, intp);
%pointer_class(double, doublep);



%include "../Headers/using_cpointer.h"
