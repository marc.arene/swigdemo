// %module swigdemo

/* Add here all the libraries and headers that are needed by your C code. Even the function */
%{
  #define SWIG_FILE_WITH_INIT
  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  #include "../Headers/using_numpy.h"
%}

%include "numpy.i"

/* initialize module*/
%init %{
  import_array();
%}

/*typemaps for the arrays, cf https://docs.scipy.org/doc/numpy-1.13.0/reference/swig.interface-file.html#beyond-the-provided-typemaps*/
/* These typemaps will be applied to all the definitions of the functions present in the header files added above */
/* INPUT ARRAY typemaps:*/
%apply (double* IN_ARRAY1, int DIM1)
{
  (double *npyInArray1D, int npyLength1D),
  (double *npyInArray1D, int npyInLength1D),
  (double *npyInArray1D_1, int npyLength1D_1),
  (double *npyInArray1D_2, int npyLength1D_2)
};
/*INPLACE ARRAY typemaps:*/
%apply (double* INPLACE_ARRAY1, int DIM1) {(double *npyInPlArray1D, int npyLength1D)};
%apply (double* INPLACE_ARRAY2, int DIM1, int DIM2) {(double* npyInPlArray2D, int npyLength1D, int npyLength2D)};
/* ARGOUT ARRAY typemaps:*/
%apply (double* ARGOUT_ARRAY1, int DIM1) {(double *npyOutArray1D, int npyOutLength1D)};

/* Add here only the header file of the function which you want to be able to call in python  */
%include "../Headers/using_numpy.h"



/* Advanced: the cnumpy_dot() function defined in /using_numpy.h only takes 3 arguments since there is no need to give the length of each array because they have to be equal. However the typemap for the INPUT ARRAY needs the definition of the C function to have one dimension specified for each INPUT ARRAY argument. So the trick is to define inline a temporary function which has the inputs needed by the typemap, check that the lengths are equal, and then return the cnumpy_dot() function by calling it with its 3 arguments. */

%rename (cnumpy_dot) cnumpy_dot_temp;
// %rename (cnumpy_add) cnumpy_add_temp;

%inline %{
  double cnumpy_dot_temp(double *npyInArray1D_1, int npyLength1D_1, double *npyInArray1D_2, int npyLength1D_2)
  {
    if (npyLength1D_1 != npyLength1D_2)
    {
      PyErr_Format(PyExc_ValueError, "Arrays of lengths (%d,%d) given", npyLength1D_1, npyLength1D_2);
      return 0.0;
    }
    return cnumpy_dot(npyInArray1D_1, npyInArray1D_2, npyLength1D_1);
  }

  // void cnumpy_add_temp(double* npyOutArray1D, int npyOutLength1D, double *npyInArray1D_1, int npyLength1D_1, double *npyInArray1D_2, int npyLength1D_2)
  // {
  //   if (npyLength1D_1 != npyLength1D_2)
  //   {
  //     PyErr_Format(PyExc_ValueError, "Arrays of lengths (%d,%d) given", npyLength1D_1, npyLength1D_2);
  //   }
  //   return cnumpy_add(npyOutArray1D, npyOutLength1D, npyInArray1D_1, npyInArray1D_2, npyLength1D_1);
  // }
%}
