// %module swigdemo

%include "carrays.i"

%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  #include "../Headers/using_carrays.h"
%}

%array_class(double, doubleArray);

%include "../Headers/using_carrays.h"
