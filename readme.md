#### Intro
- files needed to perform swig wrap
  - your `.c` and `.h` C files which you want to swig wrap
  - a `Makefile` which will have the shell commands to call swig with the appropriate options
  - an interface file, here `swigdemo.i`: it will define many options of how to wrap your source code.
  - a python `setup.py` file

#### swigdemo.i
```C
%module swigdemo
%include "cpointer.i"

%{
#include <stdio.h>
#include <stdlib.h>
#include "../Headers/hello.h"
#include "../Headers/my_pointer_functions.h"
%}

/* Create some functions for working with "int *" */
/*
%pointer_functions(int, intp);
*/

/* Wrap a class interface around an "int *" */
%pointer_class(int, intp);

%include "../Headers/hello.h"
%include "../Headers/my_pointer_functions.h"
```

- if the `#include "../Headers/hello.h"` are absent of `%{...%}`, it will compile but with some warnings (`implicit declaration of function 'hello_you'...`)
- the `%include "../Headers/hello.h"` however are necessary for the functions defined in it to be called from the `swigdemo` python package



#### Error

- If python throws the following:
```python
TypeError: in method 'cnumpy_sum', argument 1 of type 'double *'
```
whereas you passed as a first argument to your swigwrapped function `cnumpy_sum` a numpy array of doubles, it means that the typemap you used has not been understood. Hence the function `cnumpy_sum()` is available but is expecting a array of C doubles.
