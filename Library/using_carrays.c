#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../Headers/using_carrays.h"

// For the *doubleArray input, use a doubleArray() structure available thanks to the %array_class(double, doubleArray); in the interface file.
void print_array(double *doubleArray, int dim) {
  int i;
  for (i = 0; i < dim; i++) {
    printf("[%d] = %g\n", i, doubleArray[i]);
  }
}

// For the *doubleArray input, use a doubleArray() structure available thanks to the %array_class(double, doubleArray); in the interface file.
double compute_norm(double *doubleArray, int dim)
{
  double norm = 0;
  for(int i = 0; i < dim; i++)
  {
    norm = norm + doubleArray[i]*doubleArray[i];
  }
  return sqrt(norm);
}

// For the *doubleArray input, use a doubleArray() structure available thanks to the %array_class(double, doubleArray); in the interface file.
void normalize(double *doubleArray, int dim)
{
  double norm_inverse;
  norm_inverse = 1/compute_norm(doubleArray, dim);
  for(int i = 0; i < dim; i++)
  {
    doubleArray[i] *= norm_inverse;
  }
}
