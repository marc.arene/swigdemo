#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../Headers/using_numpy.h"

// Using INPUT ARRAYS with this typemaps: %apply (double* IN_ARRAY1, int DIM1) {(int *npyInArray1D, int npyLength1D)};
double cnumpy_norm(double *npyInArray1D, int npyLength1D)
{
  double norm2 = 0;
  for(int i = 0; i < npyLength1D; i++)
  {
    norm2 += npyInArray1D[i]*npyInArray1D[i];
  }
  return sqrt(norm2);
}
// Using INPUT ARRAYS with this typemaps: %apply (double* IN_ARRAY1, int DIM1) {(double *npyInArray1D_1, int npyLength1D_1), (double *npyInArray1D_2, int npyLength1D_2)}; and then doing the %rename, %inline{} thing to go from 4 inputs to only 3
double cnumpy_dot(double *npyInArray1D_1, double *npyInArray1D_2, int npyLength1D)
{
  double dot = 0;
  for(int i = 0; i < npyLength1D; i++)
  {
    dot += npyInArray1D_1[i] * npyInArray1D_2[i];
  }
  return dot;
}

// Using INPLACE ARRAYS with this typemaps: %apply (double* INPLACE_ARRAY1, int DIM1) {(double *npyInPlArray1D, int npyLength1D)};
void cnumpy_normalize_inplace(double *npyInPlArray1D, int npyLength1D)
{
  double norm_inverse;
  norm_inverse = 1/cnumpy_norm(npyInPlArray1D, npyLength1D);
  for(int i = 0; i < npyLength1D; i++)
  {
    npyInPlArray1D[i] *= norm_inverse;
  }
}

// Using INPLACE ARRAYS with this typemaps: %apply (double* INPLACE_ARRAY2, int DIM1, int DIM2) {(double* npyInPlArray2D, int npyLength1D, int npyLength2D)};
void cnumpy_normalize_inplace2D(double *npyInPlArray2D, int npyLength1D, int npyLength2D)
{
  for(int i = 0; i < npyLength1D; i++)
  {
    double norm2 = 0;
    for(int j = 0; j < npyLength2D; j++)
    {
      int nIndexJ = i * npyLength2D + j;
      norm2 += npyInPlArray2D[nIndexJ]*npyInPlArray2D[nIndexJ];
    }
    for(int j = 0; j < npyLength2D; j++)
    {
      int nIndexJ = i * npyLength2D + j;
      npyInPlArray2D[nIndexJ] *= 1/sqrt(norm2);
    }
  }
}


// Using ARGOUT ARRAYS with this typemaps: %apply (int* ARGOUT_ARRAY1, int DIM1) {(int *npyOutArray1D, int npyLength1D)};
void cnumpy_ones_argout(double* npyOutArray1D, int npyOutLength1D)
{
  for(int i = 0; i < npyOutLength1D; i++)
  {
    npyOutArray1D[i] = 1.0;
  }
}


void cnumpy_normalize_argout(double* npyOutArray1D, int npyOutLength1D, double *npyInArray1D, int npyInLength1D)
{
  double norm_inverse;
  norm_inverse = 1/cnumpy_norm(npyInArray1D, npyInLength1D);
  for(int i = 0; i < npyInLength1D; i++)
  {
    npyOutArray1D[i] = npyInArray1D[i]*norm_inverse;
  }
}

// Using 2 INPUT ARRAYS to get 1 ARGOUT ARRAY with this typemap for the input arrays:
// - %apply (double* IN_ARRAY1, int DIM1) {(double *npyInArray1D_1, int npyLength1D_1), (double *npyInArray1D_2, int npyLength1D_2)};
// and this one for the argout array:
// - %apply (double* ARGOUT_ARRAY1, int DIM1) {(double *npyOutArray1D, int npyOutLength1D)};
void cnumpy_add(double* npyOutArray1D, int npyOutLength1D, double *npyInArray1D_1, int npyLength1D_1, double *npyInArray1D_2, int npyLength1D_2)
{
  for(int i = 0; i < npyOutLength1D; i++)
  {
    npyOutArray1D[i] = npyInArray1D_1[i] + npyInArray1D_2[i];
  }
}
